﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    public partial class App : Application
    {
        /* Initialization of the mainPage*/
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
