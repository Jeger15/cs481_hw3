﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            await DisplayAlert("Alert", "Welcome on SpongeBob Page", "I want it !");
        }
        /* Fonction when the Page Disappearing*/
        protected async override void OnDisappearing()
        {
            await DisplayAlert("Alert", "You leave SpongeBob Page", "Too bad..");
        }

        /*When The user use on the slider, the picture is rotate in 2 side*/
        public void ActionPage(object sender, EventArgs e)
        {
            double invertedValue = -((sender as Slider).Value);
            this.img.RotateTo(invertedValue);
        }
    }
}