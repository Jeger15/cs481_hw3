﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        /*When The user use on the slider, the picture is rotate in 2 side*/
        public void ActionPage(object sender, EventArgs e)
        {
            double invertedValue = -((sender as Slider).Value);
            this.img.RotateTo(invertedValue);
        }
        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            await DisplayAlert("Alert", "Welcome on Patrick Page", "I want it !");
        }

        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Alert", "You leave Patrick Page", "Too bad..");
        }
    }
}