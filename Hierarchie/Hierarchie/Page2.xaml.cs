﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
            ClearStart();
        }

        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            await DisplayAlert("Alert", "Welcome on Calculator Page", "I want it !");
        }
        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Alert", "You leave Calculator Page", "Too bad..");
        }


        int statementOp;
        double first, second, tmp, result;
        string Toperator;
        /*Init variable to 0 for the begin or the end*/
        void ClearStart()
        {
            this.first = 0;
            this.second = 0;
            this.tmp = 0;
            this.result = 0;
            this.statementOp = 0;
            this.Toperator = "";
            this.ResultOutput.Text = "0";
        }

        /*Input Reset for the display on the label Output*/
        void ClearSelect(object sender, EventArgs e)
        {
            this.ResultOutput.Text = "0";
            this.result = 0;
        }

        /*When the 2 number are stock & Equal button active, Operation in progresss*/
        void EqualSelect(object sender, EventArgs e)
        {
            if (this.statementOp == 2 && this.Toperator == "+")
                this.result = this.first + this.second;
            if (this.statementOp == 2 && this.Toperator == "-")
                this.result = this.first - this.second;
            if (this.statementOp == 2 && this.Toperator == "x")
                this.result = (this.first * this.second);
            if (this.statementOp == 2 && this.Toperator == "/")
                this.result = this.first / this.second;
            this.ResultOutput.Text = this.result.ToString();
        }

        /*Stockage of the number for manipulation, change statemnt in fact of the button use*/
        void NumberSelect(object sender, EventArgs e)
        {
            Button buttonPressed = (Button)sender;
            string pressed = buttonPressed.Text;

            if (this.statementOp == 2)
                ClearStart();

            if (this.ResultOutput.Text == "0" && this.statementOp <= 0)
                this.ResultOutput.Text = "";

            if (pressed == "+/-")
                this.ResultOutput.Text = "-";
            else
                this.ResultOutput.Text += pressed;

            if (this.statementOp == -1)
            {
                this.tmp = 0;
                if (double.TryParse(this.ResultOutput.Text, out tmp))
                {
                    this.second = this.tmp;
                    this.statementOp = 2;
                }

            }
        }

        /* In fact of the Operator choice, the statemnt change for the calcul*/
        void OperatorSelect(object sender, EventArgs e)
        {
            Button buttonPressed = (Button)sender;
            string pressed = buttonPressed.Text;

            this.Toperator = pressed;

            if (this.statementOp == 0)
            {
                this.tmp = 0;
                if (double.TryParse(this.ResultOutput.Text, out this.tmp))
                {
                    this.first = this.tmp;
                    this.statementOp = -1;
                    this.ResultOutput.Text = "0";
                }
            }
        }
    }
}