﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PagePatrick : ContentPage
    {
        public PagePatrick()
        {
            InitializeComponent();
        }
        /*When The user use on the slider, the picture is rotate in 2 side*/
        public void ActionPage(object sender, EventArgs e)
        {
            double invertedValue = -((sender as Slider).Value);
            this.img.RotateTo(invertedValue);
        }
        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            bool answer = await DisplayAlert("Question? Mandatory :)", "What's the name of the plankton lady robot?", "Karine", "Karen");

            if (!answer)
            {
                await DisplayAlert("WINNER !", "Good Job True Fan.", "NEXT");
            }
            else
                await DisplayAlert("LOSER !", "Too Bad.. Question not so easy. You'll beter the next time.", "NEXT");
        }

        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Be Caruful !", "You leave Patrick Page", "Too bad..");
        }
    }
}