﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            await DisplayAlert("Welcome To Spinning'BOB Adventure !", "Ready for an adventure?", "It's about time");
        }

        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Alert !", "You leave The Main Page", "And I want That !");
        }
    }
}