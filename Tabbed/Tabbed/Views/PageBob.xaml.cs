﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageBob : ContentPage
    {
        public PageBob()
        {
            InitializeComponent();
        }
        /*When The user use on the slider, the picture is rotate in 2 side*/
        public void ActionPage(object sender, EventArgs e)
        {
            double invertedValue = -((sender as Slider).Value);
            this.img.RotateTo(invertedValue);
        }
        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            bool answer = await DisplayAlert("First Question? I trust in You.", "What's the name of the burger restaurant?", "Krusty Krab", "Rusty Krab");

            if (answer) {
                await DisplayAlert("WINNER !", "EZ but good answer", "NEXT");
            }
            else
                await DisplayAlert("LOSER !", "Too Bad.. Question easy but if you don't now, it's a life.", "NEXT");

        }

        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Be Careful !", "You leave Bob Page", "Too bad..");
        }
    }
}