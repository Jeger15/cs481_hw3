﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageCarlo : ContentPage
    {
        public PageCarlo()
        {
            InitializeComponent();
        }
        /* When The user use on the slider, the picture is rotate in 2 side */
        public void ActionPage(object sender, EventArgs e)
        {
            double invertedValue = -((sender as Slider).Value);
            this.img.RotateTo(invertedValue);
        }
        /* Fonction when the Page appearing*/
        protected override async void OnAppearing()
        {
            bool answer = await DisplayAlert("Almost the end.. Good Luck !", "Was the daughter of Mr Crab, Bob's boss, adopted?", "Yes", "No");

            if (answer)
            {
                await DisplayAlert("WINNER !", "TRUE FAN ! Enjoy the last level of Spinning'BOB", "NEXT");
            }
            else
                await DisplayAlert("LOSER !", "You've made it this far already. You should be Proud.", "NEXT");
        }

        /* Fonction when the Page Disappearing*/
        protected override async void OnDisappearing()
        {
            await DisplayAlert("Be Careful !", "You leave Carlo Page", "Too bad..");
        }
    }
}